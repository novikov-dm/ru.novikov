package ru.novikov.task1;

public class SalaryNet {

    public static void main(String[] args) {

        int salaryGross = 70000;
        int taxProcent = 13;

        int salaryNet = salaryGross - ((salaryGross * taxProcent) / 100);

        System.out.println(salaryNet);
    }
}
