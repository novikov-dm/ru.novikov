package ru.novikov.task4;

import java.util.Scanner;

public class Progression {

    private static void arithmeticProgression() {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Введите первый член аримфетической прогресси: ");
        long start = scanner.nextInt();
        System.out.print("Введите шаг арифметической прогрессии: ");
        int step = scanner.nextInt();
        System.out.print("Введите количество членов прогрессии: ");
        int n = scanner.nextInt();
        while (n > 0) {
            System.out.printf("%d ", start);
            start += step;
            n--;
        }
    }

    private static void geometricProgression() {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Введите первый член геометрической прогресси: ");
        long start = scanner.nextInt();
        System.out.print("Введите знаменатель геометрической прогрессии: ");
        int step = scanner.nextInt();
        System.out.print("Введите количество членов прогрессии: ");
        int n = scanner.nextInt();
        while (n > 0) {
            System.out.printf("%d ", start);
            start *= step;
            n--;
        }
    }


    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        System.out.print("Выбери тип прогрессии Арифметическая[1]/Геометрическая[2]: ");
        String line = scanner.nextLine().toLowerCase().trim();
        switch (line) {
            case "арифметическая":
            case "1":
            case "":
                arithmeticProgression();
                break;
            case "геометрическая":
            case "2":
                geometricProgression();
                break;
            default:
                System.out.println("Вы неверно выбрали тип прогрессии!");
        }
    }
}
