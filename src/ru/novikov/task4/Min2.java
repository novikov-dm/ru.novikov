package ru.novikov.task4;

import java.util.Scanner;

public class Min2 {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.print("Введи одно целое число: ");
        int n1 = scanner.nextInt();
        System.out.print("Введи другое целое число: ");
        int n2 = scanner.nextInt();
        int min2 = n1;
        if (n2 < min2) {
            min2 = n2;
        }
        System.out.printf("Для чисел %d и %d минимумом является %d\n", n1, n2, min2);
    }
}
