package ru.novikov.task4;

import java.util.Scanner;

public class ReverseLine {

    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        System.out.print("Введи строку: ");
        String line = scanner.nextLine();
        StringBuilder newLine = new StringBuilder();
        int length = line.length();
        for (int i = 0; i < length; i++) {
            newLine.append(line.charAt(length - 1 - i));
        }
        System.out.println(newLine);
    }
}
