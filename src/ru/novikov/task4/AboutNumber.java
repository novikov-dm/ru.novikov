package ru.novikov.task4;

import java.util.Scanner;

import static java.lang.Math.abs;


public class AboutNumber {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int number = scanner.nextInt();
        boolean isEven = abs(number) % 2 == 0;
        if (isEven) {
            if (number > 0) {
                System.out.printf("Число %d положительное и чётное.\n", number);
            } else if (number < 0) {
                System.out.printf("Число %d отрицательное и чётное.\n", number);
            } else {
                System.out.printf("Число %d неположительное и неотрицательное, но чётное.\n", number);
            }
        } else {
            if (number > 0) {
                System.out.printf("Число %d положительное и нечётное.\n", number);
            } else {
                System.out.printf("Число %d отрицательное и нечётное.\n", number);
            }
        }
    }
}
