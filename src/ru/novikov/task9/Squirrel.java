package ru.novikov.task9;

class Squirrel implements Run, Fly {

    private final String NAME = "Белка";
    private String canFly = "Я умею летать, как %s: с дерева на дерево!";

    void getName() {
        System.out.println(NAME);
    }

    @Override
    public void aboutFly() {
        System.out.printf(canFly, NAME);
    }

    @Override
    public void aboutRun() {
        System.out.printf(canRun, NAME);
    }
}
