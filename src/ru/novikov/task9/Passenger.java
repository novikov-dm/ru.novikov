package ru.novikov.task9;

public class Passenger extends Human {

    private final String NAME = "пассажир";

    @Override
    public void aboutSwim() {
        System.out.printf(canSwim, NAME);
    }

    @Override
    public void aboutRun() {
        System.out.println("Я умею бегать по палубе!");
    }

    private final String CANSAILSHIP = "Я умею плавать на корабле как пассажир!";

    @Override
    public void sailAShip() {
        System.out.println(CANSAILSHIP);
    }

    private final String CANSAILBOAT = "Я умею плавать на лодке как пассажир!";

    @Override
    public void sailABoat() {
        System.out.println(CANSAILBOAT);
    }
}
