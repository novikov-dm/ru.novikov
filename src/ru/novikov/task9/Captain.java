package ru.novikov.task9;

public class Captain extends Human {

    private final String NAME = "капитан";

    @Override
    public void aboutSwim() {
        System.out.printf(canSwim, NAME);
    }

    @Override
    public void aboutRun() {
        System.out.println("Мои мысли бегут быстрее, чем мои ноги!");
    }

    private final String CANSAILSHIP = "Я умею вести корабль по волнам как настоящий морской волк!";

    @Override
    public void sailAShip() {
        System.out.println(CANSAILSHIP);
    }

    private final String CANSAILBOAT = "Я умею управлять не только одной лодкой, но целой флотилией!";

    @Override
    public void sailABoat() {
        System.out.println(CANSAILBOAT);
    }
}
