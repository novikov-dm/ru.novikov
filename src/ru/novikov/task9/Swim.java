package ru.novikov.task9;

interface Swim {

    String canSwim = "Я умею плавать, как %s!";

    void aboutSwim();

    void sailAShip();

    void sailABoat();

}
