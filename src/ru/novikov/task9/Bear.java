package ru.novikov.task9;

class Bear implements Run, Swim {

    private final String NAME = "медведь";

    void getName() {
        System.out.println(NAME);
    }

    @Override
    public void aboutRun() {
        System.out.printf(canRun, NAME);
    }

    @Override
    public void aboutSwim() {
        System.out.printf(canSwim, NAME);
    }

    private final String HOLD = "Так можно плыть только в трюме!";

    @Override
    public void sailABoat() {
        System.out.println(HOLD);
    }

    @Override
    public void sailAShip() {
        System.out.println(HOLD);
    }
}
