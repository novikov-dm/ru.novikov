package ru.novikov.task9;

class Goose implements Fly, Swim, Run {

    private final String NAME = "Гусь";

    void getName() {
        System.out.println(NAME);
    }

    @Override
    public void aboutFly() {
        System.out.printf(canFly, NAME);
    }

    @Override
    public void aboutSwim() {
        System.out.printf(canSwim, NAME);
    }

    @Override
    public void aboutRun() {
        System.out.printf(canRun, NAME);
    }

    private final String HOLD = "Так можно плыть только в трюме!";

    @Override
    public void sailABoat() {
        System.out.println(HOLD);
    }

    @Override
    public void sailAShip() {
        System.out.println(HOLD);
    }
}
