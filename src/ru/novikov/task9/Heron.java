package ru.novikov.task9;

class Heron implements Fly, Run {

    private final String NAME = "Цапля";

    void getName() {
        System.out.println(NAME);
    }

    @Override
    public void aboutFly() {
        System.out.printf(canFly, NAME);
    }

    @Override
    public void aboutRun() {
        System.out.printf(canRun, NAME);
    }
}
