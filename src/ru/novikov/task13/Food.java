package ru.novikov.task13;

enum Food {
    CARROT,
    APPLE,
    PORRIDGE,
    FISH,
    ASPARAGUS,
    NUTS,
    STRAWBERRIES,
    CHERRIES,
    BEANS,
    BANANA,
    EGG,
    BEEF,
    BREAD,
    BROCCOLI,
    CORN,
    POTATO
}