package ru.novikov.task13;

class Child {

    private final String THANK = "Thank, mom!";
    private final String DONE = "I ate %s with pleasure!\n";
    private final String NOT_TASTY = "Not tasty food!";

    void eats(Food food) {
        try {
            testy(food);
        } catch (FoodNotTastyException error) {
            System.out.println(error.getMessage());
        }
        System.out.println(THANK);
    }

    private void testy(Food food) throws FoodNotTastyException {
        if (food == Food.PORRIDGE ||
                food == Food.ASPARAGUS ||
                food == Food.BROCCOLI) {
            throw new FoodNotTastyException(NOT_TASTY);
        }
        System.out.printf(DONE, food);
    }
}
