package ru.novikov.task13;

class FoodNotTastyException extends Exception {
    FoodNotTastyException(String message) {
        super(message);
    }
}
