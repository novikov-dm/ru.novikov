package ru.novikov.task8;

public class Counter {

    private static int count;

    public Counter() {
        count++;
    }

    public static int getCount() {
        return count;
    }
}
