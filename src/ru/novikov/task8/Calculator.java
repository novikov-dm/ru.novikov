package ru.novikov.task8;

class Calculator {

    public static int sum(int summandA, int summandB) {
        return (summandA + summandB);
    }

    public static double sum(double summandA, double summandB) {
        return (summandA + summandB);
    }


    public static int subtract(int minuend, int subtrahend) {
        return (minuend - subtrahend);
    }

    public static double subtract(double minuend, double subtrahend) {
        return (minuend - subtrahend);
    }


    public static int multiply(int factorA, int factorB) {
        return (factorA * factorB);
    }

    public static double multiply(double factorA, double factorB) {
        return (factorA * factorB);
    }


    public static int div(int dividend, int divider) {
        return (dividend / divider);
    }

    public static int mod(int dividend, int divider) {
        return (dividend / divider);
    }

    public static double division(int dividend, int divider) {
        return ((0.0 + dividend) / divider);
    }

    public static double division(double dividend, double divider) {
        return (dividend / divider);
    }


    public static int percent(int number, int part) {
        return number / 100 * part;
    }

    public static double percent(double number, double part) {
        return number / 100 * part;
    }

}
