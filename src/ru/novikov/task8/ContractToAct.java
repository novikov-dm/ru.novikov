package ru.novikov.task8;

class ContractToAct {

    static void contractToAct(Contract contract) {

        Act act = new Act();

        act.setNumber(contract.getNumber());
        act.setDate(contract.getDate());
        act.setWares(contract.getWares());
    }
}
