package ru.novikov.task8;

import java.util.Date;

class Act {
    //номер,
    private int number;
    //дата,
    private Date date;
    //список товаров (массив строк)
    private String[] wares;

    int getNumber() {
        return number;
    }

    void setNumber(int number) {
        this.number = number;
    }

    Date getDate() {
        return date;
    }

    void setDate(Date date) {
        this.date = date;
    }

    String[] getWares() {
        return wares;
    }

    void setWares(String[] wares) {
        this.wares = wares;
    }
}
