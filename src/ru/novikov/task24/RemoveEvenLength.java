package ru.novikov.task24;

import java.util.HashSet;
import java.util.Set;

public class RemoveEvenLength {

    // первый способ реализации
    public void removeEvenLength0(Set<String> set) {
        set.removeIf(s -> s.length() % 2 == 0);
    }

    // второй способ реализации
    public Set<String> removeEvenLength1(Set<String> set) {
        Set<String> newSet = new HashSet<>();
        for (String element : set) {
            if (element.length() % 2 != 0)
                newSet.add(element);
        }
        return newSet;
    }

    // третий способ
    public Set<String> removeEvenLength2(Set<String> set) {
        for (String element : set) {
            if (element.length() % 2 == 0)
                set.remove(element);
        }
        return set;
    }
}

