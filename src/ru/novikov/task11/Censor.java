package ru.novikov.task11;

class Censor {
    private String text;
    private String byaka;
    private String censorship;

    private int lenByaka;
    private String textLower;
    private String textNew;

    Censor(String text, String byaka, String censorship) {
        this.text = text;
        this.byaka = byaka;
        this.censorship = censorship;

        this.lenByaka = byaka.length();
        this.textLower = text.toLowerCase();
        this.textNew = "";
    }

    String censorText() {

        while (text.length() != 0) {
            int i = textLower.indexOf(byaka);
            if (i == -1) {
                textNew += text;
                text = "";
            } else {
                textNew += text.substring(0, i);
                textNew += censorship;
                text = text.substring(i + lenByaka);
                textLower = textLower.substring(i + lenByaka);
            }
        }

        return textNew;
    }
}

