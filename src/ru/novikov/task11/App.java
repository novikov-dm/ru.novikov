package ru.novikov.task11;

import java.util.Scanner;

public class App {
    public static void main(String[] argv) {

        Scanner scanner = new Scanner(System.in);

        String text = scanner.nextLine();
        String byaka = "бяка";
        String censorship = "[Вырезано цензурой]";

        Censor censor = new Censor(text, byaka, censorship);

        text = censor.censorText();

        System.out.println(text);
    }
}
