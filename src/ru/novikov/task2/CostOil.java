package ru.novikov.task2;

import java.util.Scanner;

public class CostOil {

    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);

        System.out.print("Введите цену одного литра бензина: ");
        int priceOil = scanner.nextInt();
        System.out.print("Введите количество литров бензина: ");
        int volumeOil = scanner.nextInt();

        int costOil = priceOil * volumeOil;

        System.out.println(volumeOil + "л. бензина стоит " + costOil + ".");
    }
}
