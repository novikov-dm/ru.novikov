package ru.novikov.task2;

import java.util.Scanner;

public class SalaryNet {

    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);

        System.out.print("Введите размер зарплаты до вычета налога: ");
        int salaryGross = scanner.nextInt();
        System.out.print("Введите процент налога: ");
        int taxProcent = scanner.nextInt();

        //вычисляем зарплату за вычетом налога (процент налога округляется в меньшую сторону)
        int salaryNet = salaryGross - ((salaryGross * taxProcent) / 100);

        System.out.println("Зарплата на руки составляет " + salaryNet + ".");
    }
}
