package ru.novikov.task2;

import java.util.Random;
import java.util.Scanner;

import static java.lang.Math.abs;

public class GameHotCold {
    public static void main(String[] args) {

        System.out.println("Это игра \"Горячо-Холодно\"!\n");

        Random random = new Random();
        int start = 1; // начало диапазона
        int stop = 100; // конец диапазона
        int number = start + random.nextInt(stop - start + 1); // генерируем случайное число в заданном диапазоне

        int usersNumber = 0;
        int length = stop; // инициализируем начальное расстояние до задуманного числа
        String answer;

        Scanner scanner = new Scanner(System.in);

        System.out.println("Компьютер задумал случайное число!");
        do {
            System.out.print("Введи целое число: ");
            answer = scanner.nextLine();
            if ("выход".equals(answer) || answer.isEmpty()) {
                break;
            } else if (answer.matches("[-+]?\\d+")) {
                usersNumber = Integer.parseInt(answer);
                int newLength = abs(number - usersNumber); // вычисляем новое расстояние до задуманного числа
                if (length < newLength) {
                    System.out.println("холодно");
                } else if (length >= newLength) {
                    System.out.println("горячо");
                    length = newLength;
                }
            } else {
                System.out.println("Ошибка ввода!");
            }
        } while (usersNumber != number);
        if (!(answer.equals("выход") || answer.isEmpty())) {
            System.out.println("Вы угадали задуманное число! Поздравляем!");
        } else {
            System.out.println("Игра закончена...");
        }
    }
}
