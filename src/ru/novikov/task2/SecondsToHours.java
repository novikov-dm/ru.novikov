package ru.novikov.task2;

import java.util.Scanner;

public class SecondsToHours {

    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);

        System.out.print("Введите количество секунд: ");
        int sec = scanner.nextInt();
        int days = sec / 3600 / 24;
        int hours = sec / 3600 % 24;
        int minutes = sec / 60 % 60;
        sec %= 60;

        System.out.printf("%d д. %02d:%02d:%02d\n", days, hours, minutes, sec);
    }
}
