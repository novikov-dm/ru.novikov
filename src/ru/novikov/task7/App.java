package ru.novikov.task7;

public class App {

    private static int choiceDrinks;
    private static Drink[] drinks;
    private static VendingMachine vendingMachine;
    private static String userChoice;
    private static final String DONE = "done";
    private static final String CANCEL = "cancel";
    private static final String MAKEYOURCHOICE = "Выберите напиток: ";

    public static void main(String[] args) {

        choiceDrinks = 1;
        drinks = Drink.values();
        vendingMachine = new VendingMachine(drinks);


        vendingMachine.showMenu();


        vendingMachine.addMoney(vendingMachine.getScanner().nextInt());
        userChoice = DONE;
        while (!(userChoice.equals(CANCEL)) && choiceDrinks != 0) {
            System.out.print(MAKEYOURCHOICE);
            userChoice = vendingMachine.getScanner().next().toLowerCase();
            if (userChoice.equals(CANCEL)) {
                break;
            } else {
                choiceDrinks = vendingMachine.giveMeADrink(Integer.parseInt(userChoice));
            }
        }
    }
}
