package ru.novikov.task7;

import java.util.Scanner;

public class VendingMachine {

    private Scanner scanner = new Scanner(System.in);
    private Drink[] drinks;
    private int numberOfDrink;
    private int money;

    private final String HEADERMENU = "Для выбора напитка внесите деньги и нажмите кнопку\n" +
            "________________________________________________\n" +
            "|                  |            |              |\n" +
            "|   Наименование   |    Цена    |    Кнопка    |\n" +
            "|__________________|____________|______________|\n";
    private final String BODYMENU = "|%17s |%7d     |%7d       |\n";
    private final String FOOTERMENU = "|------------------|------------|--------------|\n" +
            "Внесите деньги...";

    private final String YOURMONEY = "Вы внесли %d рублей.\n";

    private final String DRINKDONE = "Ваш %s готов!\n";
    private final String RETURNMONEY = "Не забудьте сдачу! %d руб.\n";
    private final String ADDMOREMONEY = "%s стоит %d рублей.\n" +
            "Добавьте  денег и выберите напиток ещё раз!\n";
    private final String CHOICEYOURDRINK = "Досадная опечатка?\nВыберите напиток из списка ещё раз!\n";

    //методы
    public void showMenu() {
        System.out.println(HEADERMENU);
        for (int i = 0; i < this.drinks.length; i++) {
            System.out.printf(BODYMENU, drinks[i].getTitle(), drinks[i].getPrice(), i);
        }
        System.out.println(FOOTERMENU);
    }

    public void addMoney(int money) {
        this.money += money;
        System.out.printf(YOURMONEY, this.money);
    }

    public int giveMeADrink(int numberOfDrink) {
        this.numberOfDrink = numberOfDrink;
        if (this.numberOfDrink >= 0 && this.numberOfDrink < this.drinks.length) {
            if (drinks[this.numberOfDrink].getPrice() <= this.money) {
                System.out.printf(DRINKDONE, drinks[this.numberOfDrink].getTitle());
                this.money -= drinks[this.numberOfDrink].getPrice();
                if (this.money > 0) {
                    System.out.printf(RETURNMONEY, this.money);
                }
                return 0;
            } else {
                System.out.printf(ADDMOREMONEY, drinks[this.numberOfDrink].getTitle(), drinks[this.numberOfDrink].getPrice());
                return 1;
            }
        } else {
            System.out.printf(CHOICEYOURDRINK);
            return 1;
        }
    }


    public VendingMachine(Drink[] drinks) {
        this.money = 0;
        this.drinks = drinks;
    }

    public Scanner getScanner() {
        return scanner;
    }
}

