package ru.novikov.task7;

public enum Drink {

    GREENTEA("Зелёный чай", 50),
    BLACKTEA("Чёрный чай", 50),
    ESPRESSO("Эспрессо", 70),
    AMERICANO("Американо", 110),
    CAPPUCCINO("Капучино", 130),
    LATTE("Латте", 130),
    MOCACCINO("Моккачино", 170),
    HOTCHOCOLATE("Горячий шоколад", 170);


    private String title;
    private int price;


    Drink(String title, int price) {
        this.title = title;
        this.price = price;
    }

    public String getTitle() {
        return title;
    }

    public int getPrice() {
        return price;
    }


}
