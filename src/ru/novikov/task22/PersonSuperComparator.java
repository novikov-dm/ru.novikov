package ru.novikov.task22;

import java.util.Comparator;

public class PersonSuperComparator implements Comparator<Person> {

    @Override
    public int compare(Person person1, Person person2) {
        if (person1.getAge() - person2.getAge() != 0)
            return person1.getAge() - person2.getAge();
        else
            return person1.getName().compareTo(person2.getName());
    }
}
