package ru.novikov.task25;

import jdk.nashorn.internal.objects.StringIterator;

import java.util.*;

public class BasketMapImpl implements Basket {
    private Map<String, Integer> map = new HashMap<>();

    @Override
    public void addProduct(String product, int quantity) {
        map.put(product, quantity);
    }

    @Override
    public void removeProduct(String product) {
        map.remove(product);
    }

    @Override
    public void updateProductQuantity(String product, int quantity) {
        map.replace(product, quantity);
    }

    @Override
    public void clear() {
        map.clear();
    }

    @Override
    public List<String> getProducts() {
        return new ArrayList<>(map.keySet());
    }

    @Override
    public int getProductQuantity(String product) {
        return map.get(product);
    }
}
