package ru.novikov.task25;

import java.util.Map;

class Unique {
    public boolean isUnique(Map<String, String> map) {
        for (String value0:
             map.values()) {
            int k = 0;
            for (String value1 :
                    map.values()) {
                if (value0.equals(value1))
                    k++;
                if (k > 1)
                    return false;
            }
        }
        return true;
    }
}
