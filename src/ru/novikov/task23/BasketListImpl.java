package ru.novikov.task23;

import ru.novikov.task25.Basket;

import java.util.ArrayList;
import java.util.List;

public class BasketListImpl implements Basket {
    private List<Position> positionList = new ArrayList<>();

    @Override
    public void addProduct(String product, int quantity) {
        Position position = getPosition(product);
        if (position == null) {
            position = new Position(product, 0);
        }
        position.setCount(position.getCount() + quantity);
    }

    @Override
    public void removeProduct(String product) {
        Position position = getPosition(product);
        positionList.remove(position);
    }

    @Override
    public void updateProductQuantity(String product, int quantity) {
        Position position = getPosition(product);
        position.setCount(quantity);
    }

    @Override
    public void clear() {
        positionList.clear();
    }

    @Override
    public List<String> getProducts() {
        List<String> list = new ArrayList<>();
        for (Position position : positionList) {
            list.add(position.getName());
        }
        return list;
    }

    @Override
    public int getProductQuantity(String product) {
        Position position = getPosition(product);
        return position.getCount();
    }

    private Position getPosition(String product) {
        for (Position position : positionList) {
            if (position.getName().equals(product))
                return position;
        }
        return null;
    }
}
