package ru.novikov.task6;


import java.util.ArrayList;


public class RussianLinguageTeacher extends Teacher {

    private int length = this.getStressDictionary().length;
    private int n = random.nextInt(length);


    public void getOneTestStress() {

        String[] words = this.getStressDictionary()[n].split(" ");
        String wordQuestion = words[0];
        String wordAnswer = words[1];

        final String QUESTION = "Перепечатай слово, выделив ударную гласную заглавной буквой!\n%s\n";

        setAnswer(wordAnswer);
        setQuestion(String.format(QUESTION, wordQuestion));
        setAnswerForUser(wordAnswer);

        test();
    }

    public void getMoreTestStress(int count) {

        ArrayList<Integer> wordsNumberOrdered = new ArrayList<>();
        for (int i = 0; i < length; i++) {
            wordsNumberOrdered.add(i);
        }
        ArrayList<Integer> wordsNumberUnordered = new ArrayList<>();
        for (int i = 0; i < length; i++) {
            int number = random.nextInt(wordsNumberOrdered.size());
            wordsNumberUnordered.add(wordsNumberOrdered.get(number));
        }

        this.setMark(0);
        if (count > length) {
            for (n = 0; n < length; n++) {
                getOneTestStress();
            }
        } else {
            for (n = 0; n < count; n++) {
                getOneTestStress();
            }
        }
        System.out.printf(this.getVERDICT(), this.getMark());
    }
}
