package ru.novikov.task6;

import java.util.Random;
import java.util.Scanner;


public class Teacher {

    protected Scanner scanner = new Scanner(System.in);
    protected Random random = new Random();

    private final String YES = "Верно!";
    private final String NO = "Неверно!";
    private final String VERDICT = "Ваша оценка: %d";

    private String answer;
    private String question;
    private String answerForUser;


    private int mark = 0;


    private String[] stressDictionary = new String[]{
            "дешевизна дешевИзна",
            "досуг досУг",
            "жалюзи жалюзИ",
            "иксы Иксы",
            "каталог каталОг",
            "квартал квартАл",
            "средства срЕдства",
            "красивее красИвее",
            "кухонный кУхонный",
            "оптовый оптОвый",
            "сливовый слИвовый",
            "воспринять воспринЯть",
            "вручит вручИт",
            "занял зАнял",
            "звонит звонИт",
            "исчерпать исчЕрпать",
            "опошлить опОшлить",
            "осведомишься освЕдомишься",
            "избалованный избалОванный",
            "понявший понЯвший",
            "завидно завИдно",
    };

    public String getYES() {
        return YES;
    }

    public String getNO() {
        return NO;
    }

    public String getVERDICT() {
        return VERDICT;
    }

    public int getMark() {
        return mark;
    }

    public void setMark(int mark) {

        this.mark = mark;
    }

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public String getAnswerForUser() {
        return answerForUser;
    }

    public void setAnswerForUser(String answerForUser) {
        this.answerForUser = answerForUser;
    }

    public String[] getStressDictionary() {

        return stressDictionary;
    }

    public void test() {
        System.out.printf(this.question);
        if (scanner.next().trim().equals(this.answer)) {
            System.out.println(this.getYES());
            this.setMark(this.getMark() + 1);
        } else {
            System.out.println(this.getNO());
            System.out.printf(this.answerForUser);
        }
    }
}
