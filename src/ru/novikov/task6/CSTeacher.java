package ru.novikov.task6;


public class CSTeacher extends Teacher {
    public void getOneTestDecimalToBinary() {

        final String DECIMAL_TO_BINARY = "%d = %s\n";
        final String QUESTION = "Запиши десятичное число в двоичной системе счисления\n%d = ";

        int decimalNumber = random.nextInt(254) + 1;
        String binaryNumber = Integer.toBinaryString(decimalNumber);

        setAnswer(binaryNumber);
        setQuestion(String.format(QUESTION, decimalNumber));
        setAnswerForUser(String.format(DECIMAL_TO_BINARY, decimalNumber, binaryNumber));

        test();
    }


    public void getMoreTestDecimalToBinary(int count) {
        this.setMark(0);
        for (int i = 0; i < count; i++) {
            this.getOneTestDecimalToBinary();
        }
        System.out.printf(this.getVERDICT(), this.getMark());
    }


    public void getOneTestBinaryToDecimal() {

        final String BINARY_TO_DECIMAL = "%s = %d\n";
        final String QUESTION = "Запиши двоичное число в десятичной системе счисления\n%s = ";

        String binaryNumber = Integer.toBinaryString(random.nextInt(254) + 1);
        int decimalNumber = Integer.parseInt(binaryNumber, 2);

        setAnswer(Integer.toString(decimalNumber));
        setQuestion(String.format(QUESTION, binaryNumber));
        setAnswerForUser(String.format(BINARY_TO_DECIMAL, binaryNumber, decimalNumber));

        test();
    }


    public void getMoreTestBinaryToDecimal(int count) {
        this.setMark(0);
        for (int i = 0; i < count; i++) {
            this.getOneTestBinaryToDecimal();
        }
        System.out.printf(this.getVERDICT(), this.getMark());
    }
}
