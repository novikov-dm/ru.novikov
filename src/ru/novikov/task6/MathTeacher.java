package ru.novikov.task6;


public class MathTeacher extends Teacher {

    public void getOneTestMultiplication() {

        int factor1 = random.nextInt(10) + 1;
        int factor2 = random.nextInt(10) + 1;
        int product = factor1 * factor2;

        final String QUESTION = "%d * %d = ";
        final String MULTIPLICATION = "%d * %d = %d\n";

        setAnswer(Integer.toString(product));
        setQuestion(String.format(QUESTION, factor1, factor2));
        setAnswerForUser(String.format(MULTIPLICATION, factor1, factor2, product));

        test();
    }


    public void getMoreTestMultiplication(int count) {
        this.setMark(0);
        for (int i = 0; i < count; i++) {
            this.getOneTestMultiplication();
        }
        System.out.printf(this.getVERDICT(), this.getMark());
    }
}
